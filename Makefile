TOOLS ?= docker docker-compose helm kops kubectl
OS ?= $(shell uname -s)
BASHFILES := $(patsubst %,bash/%.completion,$(TOOLS))
BASHPREFIX ?=
BASHTARGETS := bashfiles
ZSHDEFAULTPREFIX := $(CURDIR)/zsh
ZSHFILES := $(addprefix zsh/_,$(TOOLS))
ZSHPREFIX ?= $(ZSHDEFAULTPREFIX)

ifeq ($(OS),Darwin)
BASHPREFIX := $(shell brew --prefix)
BASHTARGETS += brew-bash-completion
endif

.PHONY: bash
bash: $(BASHTARGETS)
ifeq ($(wildcard $(BASHPREFIX)/etc/bash_completion.d/),)
	$(error use your system's package manager to install bash-completion)
endif
	ln -fsv bash/*.completion "$(BASHPREFIX)/etc/bash_completion.d"
	@ echo
	@ echo '================================================================================'
	@ echo 'to enable completions in this shell add the following to your ~/.bash_profile:'
	@ echo '    [ -f "$(BASHPREFIX)/etc/bash_completion" ] && . "$(BASHPREFIX)/etc/bash_completion"'
	@ echo 'then reload your shell:'
	@ echo '    exec $$SHELL -l'
	@ echo "if this doesn't work, instead try using:"
	@ echo '    for f in $$(shopt -s nullglob; echo "$(CURDIR)/bash/*.completion"); do . "$$f"; done'
	@ echo '================================================================================'
	@ echo

.PHONY: bashfiles
bashfiles: $(BASHFILES)

bash/%:
	cd bash/ && $(MAKE) "$*"

.PHONY: brew-bash-completion
brew-bash-completion:
	brew ls --versions bash-completion > /dev/null || brew install bash-completion

.PHONY: clean
clean:
	rm $(BASHFILES) $(ZSHFILES)

.PHONY: zsh
zsh: zshfiles
ifneq ($(ZSHPREFIX),$(ZSHDEFAULTPREFIX))
	mkdir -p "$(ZSHPREFIX)"
	ln -fsv zsh/_* "$(ZSHPREFIX)"
endif
	@ echo
	@ echo '================================================================================'
	@ echo 'to enable completions in this shell add the following to your ~/.zshrc:'
	@ echo '    fpath=($(ZSHPREFIX) $$fpath)'
	@ echo '    autoload -Uz compinit && compinit -i'
	@ echo 'then reload your shell:'
	@ echo '    exec $$SHELL -l'
	@ echo '================================================================================'
	@ echo

.PHONY: zshfiles
zshfiles: $(ZSHFILES)

zsh/%:
	cd zsh/ && $(MAKE) "$*"
